var ServerConfig = require('../config.js');

Parse.Cloud.define('hello', function(req, res) {
  res.success('Hi');
});

Parse.Cloud.beforeDelete('Image', function(request, response) {
  //console.log(request.object);
  //console.log("Hello World");

  var obj = request.object.toJSON();
  //console.log(obj.image.url);
  if (obj.image.url !== undefined) {

    var image = obj.image.url;
    //console.log(image);
    Parse.Cloud.httpRequest({
            method: 'DELETE',
            url: ServerConfig.serverURL+"/files/"+image.substring(image.lastIndexOf("/")+1),
            headers: {
                "X-Parse-Application-Id": ServerConfig.appId,
                "X-Parse-Master-Key" : ServerConfig.masterKey
            }
        }).then(function(httpResponse) {
          // success
          //console.log(httpResponse.text);
        },function(httpResponse) {
          // error
          console.error('Request failed with response code ' + httpResponse.status);
        });

  }

  response.success();
});

Parse.Cloud.beforeSave("Image", function(request, response) {
  if(request.user === undefined || request.user === null ){
    response.error("Devi essere loggato per salvare un immagine.");
  }else{
      response.success();
  }

});
Parse.Cloud.beforeSave("files", function(request, response) {
  if(request.user === undefined || request.user === null ){
    response.error("Devi essere loggato per salvare un immagine.");
  }else{
    response.success();
  }

});

Parse.Cloud.afterSave("Customer", function(request) {
  Parse.Cloud.useMasterKey();
  var Album = Parse.Object.extend("Album");
  var album = new Album();
  var query = new Parse.Query(Album);
  query.equalTo("customer",request.object);
  query.equalTo("name",'General');
  query.first({
    success: function(object) {

      if(object === null || object === undefined){

        var requestJson = request.object.toJSON();
        var queryUser = new Parse.Query(Parse.User);
        queryUser.get(requestJson.user.objectId, {
            success: function(user) {
              var userACL = new Parse.ACL();
              userACL.setPublicReadAccess(true);
              userACL.setWriteAccess(user, true);
              album.setACL(userACL);
              album.set("name","General");
              album.set("customer",request.object);
              album.save(null, {
                success: function(album) {
                  var UserAlbum = Parse.Object.extend("UserAlbum");
                  var user_album = new UserAlbum();
                  user_album.set("user",user);
                  user_album.set("album",album);
                  user_album.save(null, {
                    success: function(album) {
                      console.log('New object created with objectId: ' + album.id);
                    },
                    error: function(obj,error){
                      onsole.log('Failed to create new object, with error code: ' + error.message);
                    },
                  });

                  // Execute any logic that should take place after the object is saved.
                  console.log('New object created with objectId: ' + album.id);
                },
                error: function(album, error) {
                  // Execute any logic that should take place if the save fails.
                  // error is a Parse.Error with an error code and message.
                  console.log('Failed to create new object, with error code: ' + error.message);
                }
              });
            },
            error: function(object, error) {
              console.log('Failed to create new object, with error code: ' + error.message);
            }
          });



      }
    },
    error: function(error) {
      console.log("Error: " + error.code + " " + error.message);
    }
  });

});
Parse.Cloud.beforeSave("Album", function(request, response) {
  if(!request.master){
    if(request.user === undefined || request.user === null ){
      response.error("Devi essere loggato.");
      return false;
    }else{

        var Customer = Parse.Object.extend("Customer");
        var query = new Parse.Query(Customer);
        query.equalTo("user",request.user);
        query.equalTo("active",true);
        query.first({
          success: function(object) {
            if(object === null || object === undefined){
              response.error("Devi essere un cliente attivo per creare un album.");
              return false;
            }else{

              var Album = Parse.Object.extend("Album");
              var query = new Parse.Query(Album);
              query.equalTo("customer",object);
              query.count({
                success: function(count) {
                  console.log(count);
                  if(count < 100){
                    response.success();
                  }else{
                    response.error("Ogni cliente può avere al massimo 100 Album .");
                    return false;
                  }
                },
                error: function(error) {
                  console.log("Error: " + error.code + " " + error.message);
                  response.error("Errore salvataggio.");
                }
              });

            }
          },
          error: function(error) {
            console.log("Error: " + error.code + " " + error.message);
            response.error("Errore salvataggio.");
          }
        });
      }
    }else{
      var _Customer = Parse.Object.extend("Customer");
      var _query = new Parse.Query(_Customer);
      var jsonObj = request.object.toJSON();
      _query.include('user');
      _query.get(jsonObj.customer.objectId,{
        success: function(customer) {
          var user = customer.get("user");
          var acl = generateUserACL(user);
          request.object.setACL(acl);
        },
        error: function(object, error) {
          console.log("Error: " + error.code + " " + error.message);
          response.error("Errore salvataggio.");
        }
      });


      response.success();
    }


});
Parse.Cloud.afterSave("Album", function(request, response) {
  Parse.Cloud.useMasterKey();


        var UserAlbum = Parse.Object.extend("UserAlbum");
        var user_album = new UserAlbum();
        user_album.set("user",request.user);
        user_album.set("album",request.object);
        user_album.save(null, {
          success: function(album) {
            console.log('New object created with objectId: ' + album.id);
          },
          error: function(obj,error){
            onsole.log('Failed to create new object, with error code: ' + error.message);
          },
        });


});

Parse.Cloud.beforeSave("UserAlbum", function(request, response) {
  if(!request.master){
      if(request.user === undefined || request.user === null ){
        response.error("Devi essere loggato.");
        return false;
      }else{
        var user = request.object.get("user");
        if(user === undefined  || user === null){
          response.error("Utente non valido !");
          return false;
        }
         var UserAlbum = Parse.Object.extend("UserAlbum");
         var query = new Parse.Query(UserAlbum);
         query.equalTo("user",request.object.get("user"));
         query.equalTo("album",request.object.get("album"));
         query.first({
           success: function(object) {
             if(object !== null && object !== undefined){
               response.error("Esiste già questo utente nell'album.");
               return false;
             }
           },
           error: function(error) {
             console.log("Error: " + error.code + " " + error.message);
             response.error("Errore server.");
           }
         });

            var Customer = Parse.Object.extend("Customer");
            query = new Parse.Query(Customer);
            query.equalTo("user",request.user);
            query.equalTo("active",true);
            query.first({
              success: function(object) {
                if(object === null || object === undefined){
                  response.error("Devi essere un cliente attivo per cambiare gli album di un utente.");
                  return false;
                }else{
                  response.success();
                }
              },
              error: function(error) {
                console.log("Error: " + error.code + " " + error.message);
                response.error("Errore salvataggio.");
              }
            });
          }
  }else{
    var _user = request.object.get("user");
    if(_user === undefined  || _user === null){
      response.error("Utente non valido !");
      return false;
    }else{
       var _UserAlbum = Parse.Object.extend("UserAlbum");
       var _query = new Parse.Query(_UserAlbum);
       _query.equalTo("user",request.object.get("user"));
       _query.equalTo("album",request.object.get("album"));
       _query.first({
         success: function(object) {
           if(object !== null && object !== undefined){
             response.error("Esiste già questo utente nell'album.");
             return false;
           }else{
             response.success();
           }
         },
         error: function(error) {
           console.log("Error: " + error.code + " " + error.message);
           response.error("Errore server.");
         }
       });
     }

  }


});

Parse.Cloud.beforeDelete("Album", function(request, response) {
  query = new Parse.Query("Image");
  query.equalTo("album", request.object);
  query.count({
    success: function(count) {
      if (count > 0) {
        response.error("Non puoi eliminare l'album, contiene delle immagini.");
      } else {
        response.success();
      }
    },
    error: function(error) {
      response.error("Errore server" + error.code + " : " + error.message + ".");
    }
  });
});

Parse.Cloud.afterDelete("Album", function(request, response) {
   query = new Parse.Query("UserAlbum");
   query.equalTo("album", request.object);
   query.find({
     success: function(useralbums) {
       Parse.Object.destroyAll(useralbums, {
         success: function() {},
         error: function(error) {
           console.error("Error deleting related userAlbums " + error.code + ": " + error.message);
         }
       });
     },
     error: function(error) {
       console.error("Error finding related userAlbums " + error.code + ": " + error.message);
     }
   });
});

function generateUserACL(user) {
    var userACL = new Parse.ACL();
    userACL.setPublicReadAccess(true);
    userACL.setWriteAccess(user, true);

    return userACL;
}
